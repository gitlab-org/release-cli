//go:build !windows
// +build !windows

package app_test

import (
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/sirupsen/logrus"
	testlog "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-cli/internal/app"
	"gitlab.com/gitlab-org/release-cli/internal/testdata"
	"gitlab.com/gitlab-org/release-cli/internal/testhelpers"
)

func TestHTTPSCustomCA_Unix(t *testing.T) {
	chdirSet := false

	chdir := testhelpers.ChdirInPath(t, "../", &chdirSet)
	defer chdir()

	s := newUnstartedTLSServer(t, "testdata/certs/localhost.pem", "testdata/certs/localhost.key", handler(t, testdata.ResponseCreateReleaseSuccess))

	s.StartTLS()
	defer s.Close()

	tempDir := t.TempDir()

	tests := []struct {
		name           string
		certFlags      []string
		wantErrStr     string
		wantLogEntries []string
		ciBuildsDir    string
		createCert     bool
	}{
		{
			name:       "with_invalid_path_to_file",
			certFlags:  []string{"--additional-ca-cert-bundle", "unknown/CA.pem"},
			wantErrStr: "no such file or directory",
		},
		{
			name: "with_symlink_outside_of_context_is_invalid",
			// ln -s /etc/ssl/cert.pem symlink_outside.pem
			certFlags:  []string{"--additional-ca-cert-bundle", "testdata/certs/symlink_outside.pem"},
			wantErrStr: "internal/etc/ssl/cert.pem: no such file or directory",
		},
		{
			name: "with_correct_symlink_is_valid",
			// ln -s CA.pem symlink.pem
			certFlags:      []string{"--additional-ca-cert-bundle", "testdata/certs/symlink.pem"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name:           "path_traversal",
			certFlags:      []string{"--additional-ca-cert-bundle", "../testdata/certs/CA.pem"},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
		},
		{
			name:           "with_absolute_path_in_ci_builds_dir",
			certFlags:      []string{"--additional-ca-cert-bundle", filepath.Join(tempDir, "CA.pem")},
			wantLogEntries: []string{"Creating Release...", "release created successfully!"},
			ciBuildsDir:    filepath.Dir(tempDir),
			createCert:     true,
		},
		{
			name:        "with_absolute_path_outside_ci_builds_dir",
			certFlags:   []string{"--additional-ca-cert-bundle", "/tmp/other/CA.pem"},
			wantErrStr:  "open /tmp/builds/other/CA.pem: no such file or directory",
			ciBuildsDir: "/tmp/builds",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			log, hook := testlog.NewNullLogger()

			t.Setenv("CI_BUILDS_DIR", tt.ciBuildsDir)

			if tt.createCert {
				src, err := os.Open("testdata/certs/CA.pem")
				require.NoError(t, err)

				dest, err := os.Create(tt.certFlags[1])
				require.NoError(t, err)

				_, err = io.Copy(dest, src)
				require.NoError(t, err)

				err = src.Close()
				require.NoError(t, err)

				err = dest.Close()
				require.NoError(t, err)

				t.Cleanup(func() {
					err = os.Remove(dest.Name())
					require.NoError(t, err)
				})
			}

			testApp := app.New(logrus.NewEntry(log), t.Name())
			args := []string{"release-cli", "--server-url", s.URL, "--job-token",
				"token", "--project-id", "projectID"}

			args = append(args, tt.certFlags...)

			args = append(args, "create", "--name", "release name", "--description",
				"release description", "--tag-name", "v1.1.0")

			err := testApp.Run(args)
			if tt.wantErrStr != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tt.wantErrStr)
			} else {
				require.NoError(t, err)
			}

			require.ElementsMatch(t, toStrSlice(t, hook.AllEntries()), tt.wantLogEntries)
		})
	}
}
